package de.icnhtest.apk_expansion_files_test

import com.google.android.vending.expansion.downloader.impl.DownloaderService

// You must use the public key belonging to your publisher account
const val BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzBbXBymJZzR9AJL5Ht/lxC4qV/UAfUJx1gF9aem4AkwaYV1dwPz1Jsf34oWu5Ka2tyXk/HeLjNMNcN8SKmMMVVqRVgNLdBqMRv257aVoMkmDQum1nRoVeX0Aj4nX7Jus4nWkaFF8exxArFZuzH/3zniFDfxTAuZpYRSeaUHR1dBjArLQberaTiVNc0RVmD+sWkLIWewy7KKp7lWfsviatFqhQwn56pEmsYjbtrgKk9SmVSfZWRvc/jFYCu8Gk/2IJdySTgJ0gpM9qe8zkfAucZ5581HvL/osptHixJsdhIT1VGRwBZGm3irygY6aC8Fm78FdARARzKuO5ZTfR/rY2QIDAQAB"
// You should also modify this salt
val SALT = byteArrayOf(
    1, 42, -12, -1, 54, 98, -100, -12, 43, 2,
    -8, -4, 9, 5, -106, -107, -33, 45, -1, 84
)


class SampleDownloaderService: DownloaderService() {

    override fun getPublicKey(): String = BASE64_PUBLIC_KEY

    override fun getSALT(): ByteArray = SALT

    override fun getAlarmReceiverClassName(): String = SampleAlarmReceiver::class.java.name

}