package de.icnhtest.apk_expansion_files_test

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Messenger
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.android.vending.expansion.downloader.*
import com.google.android.vending.expansion.downloader.Constants.EXP_PATH
import java.io.File
import com.google.android.vending.expansion.downloader.impl.DownloadsDB.LOG_TAG
import com.google.android.vending.expansion.downloader.Helpers.doesFileExist
import com.google.android.vending.expansion.downloader.Helpers.getExpansionAPKFileName


class MainActivity : AppCompatActivity(), IDownloaderClient {


    override fun onDownloadStateChanged(newState: Int) {
    }

    override fun onDownloadProgress(progress: DownloadProgressInfo?) {
    }

    private var remoteService: IDownloaderService? = null

    override fun onServiceConnected(m: Messenger?) {
        remoteService = DownloaderServiceMarshaller.CreateProxy(m).apply {
            downloaderClientStub?.messenger?.also { messenger ->
                onClientUpdated(messenger)
            }
        } //To change body of created functions use File | Settings | File Templates.
    }


    private var downloaderClientStub: IStub? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Check if expansion files are available before going any further
        setContentView(R.layout.activity_main)

        val mainActivityTextField = findViewById<TextView>(R.id.filePathText)
        mainActivityTextField.text=getAPKExpansionFiles(this,1)?:"failed"// Expansion files are available, start the app

        val testButton = findViewById<Button>(R.id.button3)
        testButton.setOnClickListener { if(expansionFilesDelivered())mainActivityTextField.text="success" }

        if (!expansionFilesDelivered()) {
            val pendingIntent =
                // Build an Intent to start this activity from the Notification
                Intent(this, MainActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                }.let { notifierIntent ->
                    PendingIntent.getActivity(
                        this,
                        0,
                        notifierIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                }

            Log.e(LOG_TAG, "pendingIntent created.")

            // Start the download service (if required)
            val startResult: Int = DownloaderClientMarshaller.startDownloadServiceIfRequired(
                this,
                pendingIntent,
                SampleDownloaderService::class.java
            )
            Log.e(LOG_TAG, "startResult created.")
            // If download has started, initialize this activity to show
            // download progress
            if (startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
                // This is where you do set up to display the download
                // progress (next step)
                downloaderClientStub =
                    DownloaderClientMarshaller.CreateStub(this, SampleDownloaderService::class.java)
                // Inflate layout that shows download progress
                Log.e(LOG_TAG, "Stub created.")
                return
            } // If the download wasn't necessary, fall through to start the app

        }
    }


    override fun onResume() {
        downloaderClientStub?.connect(this)
        super.onResume()
    }

    override fun onStop() {
        downloaderClientStub?.disconnect(this)
        super.onStop()
    }

    private class XAPKFile internal constructor(
        val mIsMain: Boolean,
        val mFileVersion: Int,
        val mFileSize: Long
    )

    ///holds information about Expansion File
    private val expansionFile: XAPKFile = XAPKFile(
        true, // true signifies a main file
        1, // the version of the APK that the file was uploaded against
        16907L // the length of the file in bytes
    )


    ///checks if file is downloaded
    private fun expansionFilesDelivered(): Boolean {
        val fileName =
            getExpansionAPKFileName(this, expansionFile.mIsMain, expansionFile.mFileVersion)
        // Log.v(LOG_TAG, "XAPKFile name : " + fileName);
        if (!doesFileExist(this, fileName, expansionFile.mFileSize, false)) {
            Log.e(LOG_TAG, "ExpansionAPKFile doesn't exist or has a wrong size ($fileName).")
            return false
        }

        return true
    }

    ///returns paths of the expansion file
    private fun getAPKExpansionFiles(ctx: Context, mainVersion: Int): String? {
        val packageName = ctx.packageName
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            // Build the full path to the app's expansion files
            val root = Environment.getExternalStorageDirectory()
            val expPath = File(root.toString() + EXP_PATH + packageName)
            // Check that expansion file path exists
            if (expPath.exists()) {
                if (mainVersion > 0) {
                    val strMainPath = "$expPath${File.separator}main.$mainVersion.$packageName.obb"
                    val main = File(strMainPath)
                    if (main.isFile) {
                        return strMainPath
                    }
                }
            }
        }
        return null
    }


}
